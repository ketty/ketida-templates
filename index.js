const { createTemplates, getTemplates, cleanTemplatesFolder } = require("./scripts")

module.exports = {
  cleanTemplatesFolder,
  createTemplates,
  getTemplates
}
