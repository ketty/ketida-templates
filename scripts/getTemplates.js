const { execute } = require("../utils/utils")
const cleanTemplatesFolder = require('./cleanTemplatesFolder')
const path = require('path')

const getTemplates = async () => {  
  try {
    await cleanTemplatesFolder()
    await execute(`. ${path.join(__dirname, 'sh' ,'fetchTemplates.sh')}`);
  } catch (e) {
    throw new Error(e.message);
  }
}

module.exports = getTemplates
