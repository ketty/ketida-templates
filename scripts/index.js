const createTemplates = require('./seeds/createTemplates')
const cleanTemplatesFolder = require('./cleanTemplatesFolder')
const getTemplates = require('./getTemplates')

module.exports = {
  cleanTemplatesFolder,
  createTemplates,
  getTemplates
}